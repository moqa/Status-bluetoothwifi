package ir.test.status;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_gps ,txt_wifi , txt_bluetooth;
    ImageView img_view;
    Button wifi,bluetooth,gps,gallery;
    Context mContext =this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        checkWifi();
        checkBluetooth();
        checkGps();
    }


    void bind(){
        txt_bluetooth = findViewById(R.id.txt_bluetooth);
        txt_gps  = findViewById(R.id.txt_gps);
        txt_wifi = findViewById(R.id.txt_wifi);
        img_view = findViewById(R.id.img_view);
        findViewById(R.id.wifi).setOnClickListener(this);
        findViewById(R.id.bluetooth).setOnClickListener(this);
        findViewById(R.id.gps).setOnClickListener(this);
        findViewById(R.id.gallery).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.gallery) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, 5);
        }
    else if (v.getId() == R.id.wifi) {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivityForResult(intent, 7);
    }else if (v.getId() == R.id.bluetooth){
        Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivityForResult(intent, 25);
    }else {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 86);
    }

    }
    void checkWifi(){
        WifiManager wifiManager = (WifiManager)this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifiManager != null;
        if (wifiManager.getWifiState()== WifiManager.WIFI_STATE_ENABLED)
            txt_wifi.setText(R.string.checkWifi_on);
        else  txt_wifi.setText(R.string.checkWifi_off);
    }
    void checkBluetooth(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            txt_bluetooth.setText(R.string.blue_failed);
        } else {
            if (!mBluetoothAdapter.isEnabled())
                txt_bluetooth.setText(R.string.checkBle_off);
            else txt_bluetooth.setText(R.string.checkBlue_on);
        }
    }
    void checkGps() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {
            txt_gps.setText(R.string.checkGps_off);
        } else txt_gps.setText(R.string.checkGps_on);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 7)
            checkWifi();
        else if (requestCode == 25)
            checkBluetooth();
        else if (requestCode == 86)
            checkGps();
         if (requestCode == 5){
            if(resultCode == Activity.RESULT_OK){
                Uri selectedMedia = intent.getData();
                if(selectedMedia.toString().contains("image")){
                   Glide.with(mContext)
                            .load(selectedMedia)
                            .into(img_view);
                }
            }
        }

    }

}


